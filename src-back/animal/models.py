"""
Animal models
"""
from django.conf import settings
from django.db import models
from django.db.models import deletion


class Animal(models.Model):
    """
    Animal registered
    """

    # Name/Surname for animal
    name = models.CharField()
    # Guardian of animal
    guardian = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=deletion.PROTECT)
    # Date when the animal is born
    birthday_date = models.DateTimeField(null=True)
    # Date when the animal has been registered
    registered_date = models.DateTimeField(auto_now_add=True)
    # Species of animal (Cat, Pig, Snake, etc ...)
    species = models.CharField(null=True)
