"""
Animal app views
"""
import django_filters
from rest_framework import viewsets

from .models import Animal
from .serializers import AnimalSerializer


class AnimalFilter(django_filters.FilterSet):
    """
    Animal filters
    """

    class Meta:
        model = Animal
        fields = ["name", "guardian"]


class AnimalViewSet(viewsets.ModelViewSet):
    """
    Animal endpoint
    """

    # Model
    model = Animal
    queryset = model.objects.all()
    serializer_class = AnimalSerializer
    # Filter
    filterset_class = AnimalFilter
    search_fields = ["name"]
    ordering_fields = ["id"]
