import factory
from user.factories import UserFactory

from . import models


class AnimalFactory(factory.django.DjangoModelFactory):
    guardian = factory.SubFactory(UserFactory)

    class Meta:
        model = models.Animal
