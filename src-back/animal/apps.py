"""
Animal app config
"""
from django.apps import AppConfig


class AnimalConfig(AppConfig):
    """
    Animal Config
    """

    name = "animal"
