"""
Animal app serializers
"""
from rest_framework import serializers

from .models import Animal


class AnimalSerializer(serializers.ModelSerializer):
    """
    Main Animal serializers
    """

    registered_date = serializers.DateTimeField(required=False)

    class Meta:
        model = Animal
        fields = "__all__"
