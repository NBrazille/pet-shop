"""
Animal app urls
"""
from rest_framework import routers

from .views import AnimalViewSet

router = routers.DefaultRouter()
router.register(r"animals", AnimalViewSet)

urlpatterns = router.urls
