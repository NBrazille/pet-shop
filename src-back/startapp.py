"""
Utilities to generate a small app with all configuration for DRF / Pytest
"""

import os
import sys

PROJECT_DIR = "./"
SERVER_DIR = os.path.join("./", "server")


def write_file(app_dir: str, name_file: str, content: str):
    """Provide an utility fonction to create file"""
    with open(os.path.join(app_dir, name_file), "w", encoding="utf-8") as file_writer:
        file_writer.write(content)


def create_app(model_name: str):
    """
    Create a small app based on model_name
    """
    # Create the app directory
    app_name = f"{model_name.lower()}"
    app_dir = os.path.join(PROJECT_DIR, app_name)
    try:
        os.mkdir(app_dir)
    except FileExistsError:
        pass

    # Create the apps.py file
    write_file(
        app_dir,
        "__init__.py",
        "",
    )

    # Create the apps.py file
    write_file(
        app_dir,
        "apps.py",
        f"""\"\"\"
{model_name} app config
\"\"\"
from django.apps import AppConfig


class {model_name}Config(AppConfig):
    \"\"\"
    {model_name} Config
    \"\"\"

    name = "{app_name}"
""",
    )

    # Create the factories.py file
    write_file(
        app_dir,
        "factories.py",
        f"""import factory

from . import models


class {model_name}Factory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.{model_name}
""",
    )

    # Create the tests.py file
    write_file(
        app_dir,
        "tests.py",
        f"""from django.urls import reverse
from rest_framework.test import APITestCase

from .factories import {model_name}Factory
from .views import {model_name}ViewSet


class {model_name}ViewSetTest(APITestCase):
    view = {model_name}ViewSet
    factory_class = {model_name}Factory

    view_name = "{app_name}"
    url_list = reverse(f"{{view_name}}-list")

    def get_url_detail(self, elt_id: int):
        return reverse(f"{{self.view_name}}-detail", kwargs={{"pk": elt_id}})

    def test_list(self):
        # Given
        {app_name}_1 = self.factory_class.create()
        {app_name}_2 = self.factory_class.create()

        # When
        response = self.client.get(self.url_list)

        # Then
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.data,
            self.view.serializer_class(many=True).to_representation(
                [{app_name}_1, {app_name}_2]
            ),
        )

    def test_create(self):
        # When
        response = self.client.post(self.url_list, data={{"name": "new"}})

        # Then
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data["name"], "new")

    def test_detail(self):
        # Given
        {app_name} = self.factory_class.create()

        # When
        response = self.client.get(self.get_url_detail({app_name}.id))

        # Then
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.data,
            self.view.serializer_class().to_representation({app_name}),
        )

    def test_update(self):
        # Given
        {app_name} = self.factory_class.create(name="old_name")

        # When
        response = self.client.patch(
            self.get_url_detail({app_name}.id), data={{"name": "new_name"}}
        )

        # Then
        {app_name}.refresh_from_db()
        self.assertEqual(response.status_code, 200)
        self.assertEqual({app_name}.name, "new_name")

    def test_delete(self):
        # Given
        {app_name} = self.factory_class.create()

        # When
        response = self.client.delete(self.get_url_detail({app_name}.id))

        # Then
        self.assertEqual(response.status_code, 204)
        self.assertEqual(self.view.model.objects.count(), 0)
""",
    )

    # Create the models.py file
    write_file(
        app_dir,
        "models.py",
        f"""\"\"\"
{model_name} models
\"\"\"
from django.db import models


class {model_name}(models.Model):
    \"\"\"
    TODO: <Insert description of {model_name}>
    \"\"\"

    name = models.CharField(max_length=500)
""",
    )

    # Create the serializers.py file
    write_file(
        app_dir,
        "serializers.py",
        f"""\"\"\"
{model_name} app serializers
\"\"\"
from rest_framework import serializers

from .models import {model_name}


class {model_name}Serializer(serializers.ModelSerializer):
    \"\"\"
    Main {model_name} serializers
    \"\"\"

    class Meta:
        model = {model_name}
        fields = "__all__"
""",
    )

    # Create the views.py file
    views_file = os.path.join(app_dir, "views.py")
    with open(views_file, "w", encoding="utf-8") as file_writer:
        file_writer.write(
            f"""\"\"\"
{model_name} app views
\"\"\"
from rest_framework import viewsets

from .models import {model_name}
from .serializers import {model_name}Serializer


class {model_name}ViewSet(viewsets.ModelViewSet):
    \"\"\"
    {model_name} endpoint
    \"\"\"

    model = {model_name}
    queryset = model.objects.all()
    serializer_class = {model_name}Serializer
"""
        )

    # Create the urls.py file
    write_file(
        app_dir,
        "urls.py",
        f"""\"\"\"
{model_name} app urls
\"\"\"
from rest_framework import routers

from .views import {model_name}ViewSet

router = routers.DefaultRouter()
router.register(r"{app_name}s", {model_name}ViewSet)

urlpatterns = router.urls
""",
    )

    print(f'Successfully created new app "{app_name}" for model "{model_name}".')
    print("Remaining manuals tasks :")
    print(
        f" - Run 'python manage.py makemigrations {app_name}' then 'python manage.py migrate'"
    )
    print(f" - Add '{app_name}' to settings/INSTALLED_APPS")
    print(f' - Add \'path("api/", include("{app_name}.urls"))\' to urls/urlpatterns')


if __name__ == "__main__":
    create_app(sys.argv[1])
