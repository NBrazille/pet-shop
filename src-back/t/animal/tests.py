import datetime

import pytest
import pytz
from animal.factories import AnimalFactory
from animal.views import AnimalViewSet
from dateutil import parser
from django.urls import reverse
from rest_framework.test import APIClient
from user.factories import UserFactory

VIEWSET = AnimalViewSet
FACTORY_CLASS = AnimalFactory

VIEW_NAME = "animal"
URL_LIST = reverse(f"{VIEW_NAME}-list")


# Utils
def get_url_detail(elt_id: int):
    return reverse(f"{VIEW_NAME}-detail", kwargs={"pk": elt_id})


def datetime_to_str(date: datetime):
    if not date:
        return
    return date.replace(microsecond=0).isoformat().replace("+00:00", "Z")


# List
@pytest.mark.django_db
def test_list(api_client: APIClient):
    # Given
    animal_1 = FACTORY_CLASS.create()
    animal_2 = FACTORY_CLASS.create()

    # When
    response = api_client.get(URL_LIST)

    # Then
    assert response.status_code == 200, response.content
    assert response.data == VIEWSET.serializer_class(many=True).to_representation(
        [
            animal_1,
            animal_2,
        ]
    )


@pytest.mark.django_db
def test_list_ordering(api_client: APIClient):
    # Given
    animal_1 = FACTORY_CLASS.create()
    animal_2 = FACTORY_CLASS.create()

    # When
    response = api_client.get(URL_LIST, data={"ordering": "-id"})

    # Then
    assert response.status_code == 200, response.content
    assert response.data, VIEWSET.serializer_class(many=True).to_representation(
        [
            animal_2,
            animal_1,
        ]
    )


@pytest.mark.django_db
def test_list_filter_name(api_client: APIClient):
    # Given
    animal_1 = FACTORY_CLASS.create(name="a")
    FACTORY_CLASS.create(name="b")

    # When
    response = api_client.get(URL_LIST, data={"name": "a"})

    # Then
    assert response.status_code == 200, response.content
    assert response.data == VIEWSET.serializer_class(many=True).to_representation(
        [
            animal_1,
        ]
    )


@pytest.mark.django_db
def test_list_filter_guardian(api_client: APIClient):
    # Given
    animal_1 = FACTORY_CLASS.create()
    FACTORY_CLASS.create()

    # When
    response = api_client.get(URL_LIST, data={"guardian": animal_1.guardian.id})

    # Then
    assert response.status_code == 200, response.content
    assert response.data == VIEWSET.serializer_class(many=True).to_representation(
        [
            animal_1,
        ]
    )


# Create
@pytest.mark.django_db
def test_create_required(api_client: APIClient):
    # When
    response = api_client.post(URL_LIST)

    # Then
    assert response.status_code == 400


@pytest.mark.django_db
def test_create_minimal(api_client: APIClient):
    # Given
    guardian = UserFactory.create()
    data = {
        "name": "Meow",
        "guardian": guardian.id,
    }

    # When
    response = api_client.post(URL_LIST, data=data)

    # Then
    assert response.status_code == 201, response.content

    result = response.data
    assert result.pop("id") >= 1
    assert result["name"] == data["name"]
    assert result["guardian"] == guardian.id
    assert not result.pop("birthday_date")
    assert result.pop("registered_date")
    assert not result.pop("species")


@pytest.mark.django_db
def test_create_full(api_client: APIClient):
    # Given
    guardian = UserFactory.create()
    data = {
        "name": "Ssssss",
        "species": "snake",
        "guardian": guardian.id,
        "birthday_date": datetime_to_str(
            datetime.datetime(year=2012, month=12, day=12, tzinfo=pytz.utc)
        ),
        "registered_date": datetime_to_str(
            datetime.datetime(year=2012, month=12, day=12, tzinfo=pytz.utc)
        ),  # Should be ignored
    }

    # When
    response = api_client.post(URL_LIST, data=data)

    # Then
    assert response.status_code == 201, response.content
    result = response.data
    assert result.pop("id") >= 1
    assert (
        parser.parse(result.pop("registered_date")).year
        != parser.parse(data.pop("registered_date")).year
    )
    assert result == data


# Detail
@pytest.mark.django_db
def test_detail(api_client: APIClient):
    # Given
    animal = FACTORY_CLASS.create()

    # When
    response = api_client.get(get_url_detail(animal.id))

    # Then
    assert response.status_code == 200, response.content
    assert response.data == VIEWSET.serializer_class().to_representation(animal)


# Update
@pytest.mark.django_db
def test_update(api_client: APIClient):
    # Given
    animal = FACTORY_CLASS.create(
        **{
            "name": "wafff",
            "species": "dog",
            "birthday_date": datetime.datetime(
                year=2012, month=12, day=12, tzinfo=pytz.utc
            ),
            "registered_date": datetime.datetime(
                year=2012, month=12, day=12, tzinfo=pytz.utc
            ),
        }
    )
    new_guardian = UserFactory.create()

    new_data = {
        "name": "Ssssss",
        "species": "snake",
        "guardian": new_guardian.id,
        "birthday_date": datetime_to_str(
            datetime.datetime(year=2015, month=12, day=15, tzinfo=pytz.utc)
        ),
        "registered_date": datetime_to_str(
            datetime.datetime(year=2015, month=12, day=15, tzinfo=pytz.utc)
        ),  # Should be not ignored
    }

    # When
    response = api_client.patch(get_url_detail(animal.id), data=new_data)

    # Then
    assert response.status_code == 200, response.content

    animal.refresh_from_db()
    result = response.data
    result.pop("id")
    assert result == new_data


@pytest.mark.django_db
def test_update_no_change(api_client: APIClient):
    # Given
    animal = FACTORY_CLASS.create()
    expected = {
        "id": animal.id,
        "name": animal.name,
        "guardian": animal.guardian.id,
        "birthday_date": datetime_to_str(animal.birthday_date),
        "registered_date": datetime_to_str(animal.registered_date),
        "species": animal.species,
    }

    # When
    response = api_client.patch(get_url_detail(animal.id))

    # Then
    assert response.status_code == 200, response.content

    animal.refresh_from_db()
    result = response.data
    assert expected == result


# Delete
@pytest.mark.django_db
def test_delete(api_client: APIClient):
    # Given
    animal = FACTORY_CLASS.create()

    # When
    response = api_client.delete(get_url_detail(animal.id))

    # Then
    assert response.status_code == 204, response.content
    assert VIEWSET.queryset.count() == 0
