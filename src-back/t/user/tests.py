import pytest
from django.urls import reverse
from rest_framework.test import APIClient
from user.factories import UserFactory
from user.views import UserViewSet

VIEWSET = UserViewSet
FACTORY_CLASS = UserFactory

VIEW_NAME = "user"
URL_LIST = reverse(f"{VIEW_NAME}-list")


DEFAULT_USERNAME = "user_1"
DEFAULT_PASSWORD = "p<fàdsfss65465&@:::word"
DEFAULT_FIRST_NAME = "User"
DEFAULT_LAST_NAME = "1"


# Utils
def get_url_detail(elt_id: int):
    return reverse(f"{VIEW_NAME}-detail", kwargs={"pk": elt_id})


# List
@pytest.mark.django_db
def test_list(api_client: APIClient):
    # Given
    user_1 = FACTORY_CLASS.create()
    user_2 = FACTORY_CLASS.create()

    # When
    response = api_client.get(URL_LIST)

    # Then
    assert response.status_code == 200, response.content
    assert response.data == VIEWSET.serializer_class(many=True).to_representation(
        [
            user_1,
            user_2,
        ]
    )


@pytest.mark.django_db
def test_list_ordering(api_client: APIClient):
    # Given
    user_1 = FACTORY_CLASS.create()
    user_2 = FACTORY_CLASS.create()

    # When
    response = api_client.get(URL_LIST, data={"ordering": "-id"})

    # Then
    assert response.status_code == 200, response.content
    assert response.data, VIEWSET.serializer_class(many=True).to_representation(
        [
            user_2,
            user_1,
        ]
    )


@pytest.mark.django_db
def test_list_filter_username(api_client: APIClient):
    # Given
    user_1 = FACTORY_CLASS.create(username="a")
    FACTORY_CLASS.create(username="b")

    # When
    response = api_client.get(URL_LIST, data={"username": "a"})

    # Then
    assert response.status_code == 200, response.content
    assert response.data == VIEWSET.serializer_class(many=True).to_representation(
        [
            user_1,
        ]
    )


# Create
@pytest.mark.django_db
def test_create_required(api_client: APIClient):
    # When
    response = api_client.post(URL_LIST)

    # Then
    assert response.status_code == 400


@pytest.mark.django_db
def test_create_minimal(api_client: APIClient):
    # Given
    data = {
        "username": DEFAULT_USERNAME,
        "password": DEFAULT_PASSWORD,
    }
    expected = {
        "username": DEFAULT_USERNAME,
        "first_name": "",
        "last_name": "",
    }

    # When
    response = api_client.post(URL_LIST, data=data)

    # Then
    assert response.status_code == 201, response.content
    result = response.data
    assert result.pop("id") > 0
    assert result == expected


@pytest.mark.django_db
def test_create_full(api_client: APIClient):
    # Given
    data = {
        "username": DEFAULT_USERNAME,
        "password": DEFAULT_PASSWORD,
        "first_name": DEFAULT_FIRST_NAME,
        "last_name": DEFAULT_LAST_NAME,
    }
    expected = {
        "username": DEFAULT_USERNAME,
        "first_name": DEFAULT_FIRST_NAME,
        "last_name": DEFAULT_LAST_NAME,
    }

    # When
    response = api_client.post(URL_LIST, data=data)

    # Then
    assert response.status_code == 201, response.content
    result = response.data
    user_id = result.pop("id")
    assert result == expected
    assert VIEWSET.model.objects.get(id=user_id).check_password(DEFAULT_PASSWORD)


@pytest.mark.django_db
def test_create_duplicate(api_client: APIClient):
    # Given
    data = {
        "username": DEFAULT_USERNAME,
        "password": DEFAULT_PASSWORD,
    }
    FACTORY_CLASS.create(**data)

    # When
    response = api_client.post(URL_LIST, data=data)

    # Then
    assert response.status_code == 400, response.content
    result = response.data
    assert result == {"username": ["A user with that username already exists."]}


# Detail
@pytest.mark.django_db
def test_detail(api_client: APIClient):
    # Given
    user = FACTORY_CLASS.create()

    # When
    response = api_client.get(get_url_detail(user.id))

    # Then
    assert response.status_code == 200, response.content
    assert response.data == VIEWSET.serializer_class().to_representation(user)


# Update
@pytest.mark.django_db
def test_update(api_client: APIClient):
    # Given
    data = {
        "username": DEFAULT_USERNAME,
    }
    user = FACTORY_CLASS.create(**data)

    new_data = {
        "username": "new_user",
    }
    expected = {
        "first_name": "",
        "last_name": "",
    }
    expected.update(new_data)
    new_data["password"] = "new_password"

    # When
    response = api_client.patch(get_url_detail(user.id), data=new_data)

    # Then
    assert response.status_code == 200, response.content

    user.refresh_from_db()
    result = response.data
    user_id = result.pop("id")
    assert result == expected
    assert VIEWSET.model.objects.get(id=user_id).check_password(new_data["password"])


@pytest.mark.django_db
def test_update_no_change(api_client: APIClient):
    # Given
    user = FACTORY_CLASS.create(password=DEFAULT_PASSWORD)
    expected = {
        "username": user.username,
        "first_name": user.first_name,
        "last_name": user.last_name,
    }

    # When
    response = api_client.patch(get_url_detail(user.id))

    # Then
    assert response.status_code == 200, response.content

    user.refresh_from_db()
    result = response.data
    user_id = result.pop("id")
    assert expected == result
    assert VIEWSET.model.objects.get(id=user_id).check_password(DEFAULT_PASSWORD)


# Delete
@pytest.mark.django_db
def test_delete(api_client: APIClient):
    # Given
    user = FACTORY_CLASS.create()

    # When
    response = api_client.delete(get_url_detail(user.id))

    # Then
    assert response.status_code == 204, response.content
    assert VIEWSET.queryset.count() == 0
