"""
Global configuration for test
"""
import secrets

import pytest
from django.contrib.auth import get_user_model
from rest_framework.test import APIClient
from rest_framework_simplejwt.tokens import RefreshToken


@pytest.fixture
def api_client():
    """
    Setup API Client
    """
    # Create a test user
    user = get_user_model().objects.create_user(
        username="test",
        password=secrets.token_urlsafe(32),
        is_superuser=True,
    )
    # Init API Client
    client = APIClient()
    # Create token
    refresh = RefreshToken.for_user(user)
    # Set token
    client.credentials(HTTP_AUTHORIZATION=f"Bearer {refresh.access_token}")
    return client
