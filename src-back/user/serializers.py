"""
User app serializers
"""
from django.contrib.auth import get_user_model
from django.contrib.auth.password_validation import validate_password
from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):
    """
    Main User serializers
    """

    password = serializers.CharField(
        write_only=True, required=True, validators=[validate_password]
    )

    class Meta:
        model = get_user_model()
        fields = ["id", "username", "password", "first_name", "last_name"]

    def create(self, validated_data: dict):
        # We will manage the password later
        password = validated_data.pop("password")

        # Create user
        user = super().create(validated_data)

        # Set password for user
        user.set_password(password)
        user.save()

        return user

    def update(self, instance, validated_data: dict):
        # We will manage the password later
        password = validated_data.pop("password", None)

        # Create user
        user = super().update(instance, validated_data)

        # Set password for user
        if password:
            user.set_password(password)
            user.save()

        return user
