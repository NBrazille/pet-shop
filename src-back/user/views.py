"""
User app views
"""
import django_filters
from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework import viewsets
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework_simplejwt.exceptions import InvalidToken
from rest_framework_simplejwt.exceptions import TokenError
from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework_simplejwt.views import TokenRefreshView as JWTRefreshView

from .serializers import UserSerializer


# User
class UserFilter(django_filters.FilterSet):
    """
    User filters
    """

    class Meta:
        model = get_user_model()
        fields = [
            "username",
        ]


class UserViewSet(viewsets.ModelViewSet):
    """
    User endpoint
    """

    def get_permissions(self):
        if self.action == "create":
            return [AllowAny()]
        return super().get_permissions()

    # Model
    model = get_user_model()
    queryset = model.objects.exclude(is_superuser=True)
    serializer_class = UserSerializer
    # Filter
    filterset_class = UserFilter
    search_fields = ["username"]
    ordering_fields = ["id"]


# Token
class TokenMixin:
    """
    Mixin to override post of rest_framework_simplejwt views
    """

    def post(self, request, *_, **__):
        """
        Override post to add user representation to token
        """
        serializer = self.get_serializer(data=request.data)

        try:
            serializer.is_valid(raise_exception=True)
        except TokenError as exc:
            raise InvalidToken(exc.args[0]) from exc

        # Add user representation to data
        data = serializer.validated_data
        data.update(UserSerializer().to_representation(serializer.user))

        return Response(data, status=status.HTTP_200_OK)


class TokenView(TokenMixin, TokenObtainPairView):
    """
    Override TokenObtainPairView to add TokenMixin
    """


class TokenRefreshView(TokenMixin, JWTRefreshView):
    """
    Override JWTRefreshView to add TokenMixin
    """
