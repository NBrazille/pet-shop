"""
User app urls
"""
from django.urls import path
from rest_framework import routers

from .views import TokenRefreshView
from .views import TokenView
from .views import UserViewSet

BASE_URL = "users"

router = routers.DefaultRouter()
router.register(rf"{BASE_URL}", UserViewSet)

urlpatterns = router.urls

urlpatterns += [
    path("token/", TokenView.as_view(), name="token"),
    path("token/refresh/", TokenRefreshView.as_view(), name="token_refresh"),
]
