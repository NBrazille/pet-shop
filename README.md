# Pet Shop

## Description

This is a personal project to show my knowledge on various subjects about programming (code, integration, documentation, architecture).

I will used some concepts about pet shop to give a theme for the project. I could specialize this project for Cars, Video Games, Flower, etc ...

If you want to contributing, i will decline your help since it is a showcase of my experience and not a true project designated for production. If you have some questions/remarks/improvements, i will gladly discuss with you.

## Versions

- **Python :** 3.11.1
- **Django :** 4.2

## Tools

- [Pyenv](https://github.com/pyenv/pyenv)
- [Oh My ZSH](https://ohmyz.sh/)
- [Visual Studio Code](https://code.visualstudio.com/)

## Installation

### Setup Local (Virtual) Env

pyenv install 3.11.1
pyenv virtualenv 3.11.1 petshop
pyenv local petshop

pip install -r src-back/requirements/dev.txt

docker compose up -d

pre-commit install

## Docker help

### Check Configuration

docker compose config

### Build

docker compose build

### Up and run as deamon stack

docker compose up -d

### Stop stack

docker compose up -d

### Recreate stack

docker compose up -d --build
