export default interface IAnimal {
    id?: any | null,
    name?: string,
    registered_date?: Date,
    birthday_date?: Date,
    species?: any,
    guardian?: any,
}