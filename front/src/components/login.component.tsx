import React, { useState } from "react";
import { NavigateFunction, useNavigate } from "react-router-dom";

import { Alert, Button, Form, Image, Input, Spin } from "antd";
import EventBus from "../common/EventBus";
import * as AuthService from "../services/auth.service";

type Props = {};

const Login: React.FC<Props> = () => {
  let navigate: NavigateFunction = useNavigate();

  const [loading, setLoading] = useState<boolean>(false);
  const [messageError, setMessageError] = useState<string>("");
  const [descriptionError, setDescriptionError] = useState<string>("");

  const handleLogin = (formValue: { username: string; password: string }) => {
    const { username, password } = formValue;

    setMessageError("");
    setLoading(true);

    AuthService.login(username, password).then(
      () => {
        EventBus.dispatch("login");
        navigate("/home");
      },
      (error) => {
        const errMessage =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();

        setLoading(false);
        setMessageError(errMessage);
        setDescriptionError(error.response.data.detail);
      }
    );
  };

  return (
    <Form
      name="basic"
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 16 }}
      style={{ maxWidth: 600 }}
      initialValues={{ remember: false }}
      onFinish={handleLogin}
    >
      <Image
        src="//ssl.gstatic.com/accounts/ui/avatar_2x.png"
        alt="profile-img"
      />

      <Form.Item
        label="Username"
        name="username"
        rules={[{ required: true, message: "Please input your username !" }]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="Password"
        name="password"
        rules={[{ required: true, message: "Please input your password !" }]}
      >
        <Input.Password />
      </Form.Item>

      <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
        {loading && <Spin></Spin>}

        <Button type="primary" htmlType="submit" disabled={loading}>
          Login
        </Button>
      </Form.Item>

      {messageError && (
        <Alert
          message={messageError}
          description={descriptionError}
          type="error"
          closeIcon
        />
      )}
    </Form>
  );
};

export default Login;
