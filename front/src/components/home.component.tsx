import Title from "antd/es/typography/Title";
import React from "react";

const Home: React.FC = () => {
  return <Title>Welcome to petshop !</Title>;
};

export default Home;
