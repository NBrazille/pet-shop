import React, { useEffect, useState } from "react";

import { Table, TableProps } from "antd";
import errorManagement from "../common/ErrorHandler";
import { getUsersList } from "../services/auth.service";
import IUser from "../types/user.type";

const BoardAdmin: React.FC = () => {
  const [users, setUsers] = useState<Array<IUser>>();

  const columns = [
    {
      title: "ID",
      dataIndex: "id",
      key: "id",
    },
    {
      title: "User",
      dataIndex: "username",
      key: "username",
    },
  ];

  useEffect(() => {
    getUsersList()
      .then((response) => {
        setUsers(response.data);
      })
      .catch((error) => {
        errorManagement(error);
      });
  }, []);

  const handleChange: TableProps<IUser>["onChange"] = (
    pagination,
    filters,
    sorter
  ) => {
    console.log(pagination, filters, sorter);
  };

  return (
    <Table
      rowKey={(user) => user.id}
      columns={columns}
      dataSource={users}
      onChange={handleChange}
      pagination={{
        pageSize: 2,
        pageSizeOptions: [2, 5],
        showSizeChanger: true,
      }}
    ></Table>
  );
};

export default BoardAdmin;
