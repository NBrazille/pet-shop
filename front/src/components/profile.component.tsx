import { Descriptions } from "antd";
import React from "react";
import { getCurrentUser } from "../services/auth.service";

const Profile: React.FC = () => {
  const currentUser = getCurrentUser();

  return (
    <Descriptions
      title={"Profile " + currentUser?.username}
      layout="horizontal"
      column={1}
    >
      <Descriptions.Item label="ID">{currentUser?.id}</Descriptions.Item>

      <Descriptions.Item label="Token">
        {currentUser?.access.substring(0, 20)} ...{" "}
        {currentUser?.access.substr(currentUser?.access.length - 20)}
      </Descriptions.Item>
    </Descriptions>
  );
};

export default Profile;
