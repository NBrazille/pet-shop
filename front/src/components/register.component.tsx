import { Alert, Button, Form, Image, Input, Spin } from "antd";
import React, { useState } from "react";
import { NavigateFunction, useNavigate } from "react-router-dom";

import EventBus from "../common/EventBus";
import * as AuthService from "../services/auth.service";

import { useForm } from "antd/es/form/Form";

type Props = {};

const Register: React.FC<Props> = () => {
  let navigate: NavigateFunction = useNavigate();

  const [form] = useForm();
  const [invalid, setInvalid] = useState<boolean>(true);

  const handleFormChange = () => {
    const hasErrors = form.getFieldsError().some(({ errors }) => errors.length);
    setInvalid(hasErrors);
  };

  const [loading, setLoading] = useState<boolean>(false);
  const [messageError, setMessageError] = useState<string>("");
  const [descriptionError, setDescriptionError] = useState<string>("");

  const handleRegister = (formValue: {
    username: string;
    password: string;
  }) => {
    const { username, password } = formValue;

    AuthService.register(username, password).then(
      () => {
        AuthService.login(username, password).then(
          () => {
            EventBus.dispatch("login");
            navigate("/home");
          },
          (error) => {
            const errMessage =
              (error.response &&
                error.response.data &&
                error.response.data.message) ||
              error.message ||
              error.toString();

            setLoading(false);
            setMessageError(errMessage);
            setDescriptionError(error.response.data.detail);
          }
        );
      },
      (error) => {
        const errMessage =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();

        setLoading(false);
        setMessageError(errMessage);
        setDescriptionError(error.response.data.detail);
      }
    );
  };

  return (
    <Form
      name="basic"
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 16 }}
      style={{ maxWidth: 600 }}
      initialValues={{ remember: false }}
      form={form}
      onFieldsChange={handleFormChange}
      onFinish={handleRegister}
      autoComplete="off"
    >
      <Image
        src="//ssl.gstatic.com/accounts/ui/avatar_2x.png"
        alt="profile-img"
      />

      <Form.Item
        label="Username"
        name="username"
        rules={[{ required: true }, { min: 3 }, { max: 20 }]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="Password"
        name="password"
        rules={[{ required: true }, { min: 6 }, { max: 40 }]}
      >
        <Input.Password />
      </Form.Item>

      <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
        {loading && <Spin></Spin>}

        <Button type="primary" htmlType="submit" disabled={loading || invalid}>
          Sign Up
        </Button>
      </Form.Item>

      {messageError && (
        <Alert
          message={messageError}
          description={descriptionError}
          type="error"
          closeIcon
        />
      )}
    </Form>
  );
};

export default Register;
