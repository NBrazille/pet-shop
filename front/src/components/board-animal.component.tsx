import React, { useEffect, useState } from "react";
import IAnimal from "../types/animal.type";

import { Table, TableProps } from "antd";
import errorManagement from "../common/ErrorHandler";
import { getAnimalList } from "../services/animal.service";

const BoardAnimal: React.FC = () => {
  const [animals, setAnimals] = useState<Array<IAnimal>>();

  const columns = [
    {
      title: "ID",
      dataIndex: "id",
      key: "id",
    },
    {
      title: "Nom",
      dataIndex: "name",
      key: "name",
    },
  ];

  useEffect(() => {
    getAnimalList()
      .then((response) => {
        setAnimals(response.data);
      })
      .catch((error) => {
        errorManagement(error);
      });
  }, []);

  const handleChange: TableProps<IAnimal>["onChange"] = (
    pagination,
    filters,
    sorter
  ) => {
    console.log(pagination, filters, sorter);
  };

  return (
    <Table
      rowKey={(animal) => animal.id}
      columns={columns}
      dataSource={animals}
      onChange={handleChange}
      pagination={{
        pageSize: 2,
        pageSizeOptions: [2, 5],
        showSizeChanger: true,
      }}
    ></Table>
  );
};

export default BoardAnimal;
