export default function authHeader() {
    let user = null;

    const userStr = localStorage.getItem("user");
    if (userStr) {
        user = JSON.parse(userStr);
    }

    if (user && user.access) {
        return {
            Authorization: 'Bearer ' + user.access
        };
    } else {
        return {
            Authorization: ''

        };
    }
}
