import axios from "axios";
import authHeader from "./auth-headers";

const API_URL = process.env.REACT_APP_BACK_API + "/animals/";


export const getAnimalList = () => {
    return axios.get(API_URL, { headers: authHeader() });
};
