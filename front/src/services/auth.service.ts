import axios from "axios";
import authHeader from "./auth-headers";

const USER_URL = process.env.REACT_APP_BACK_API + "/users/";
const TOKEN_URL = process.env.REACT_APP_BACK_API + "/token/";

export const register = (username: string, password: string) => {
    return axios.post(USER_URL, {
        username,
        password,
    });
};

export const login = async (username: string, password: string) => {
    const response = await axios
        .post(TOKEN_URL, {
            username,
            password,
        });
    if (response.data.access) {
        localStorage.setItem("user", JSON.stringify(response.data));
    }
    return response.data;
};

export const exist = (userId: string) => {
    return axios.get(USER_URL + userId, { headers: authHeader() });
};

export const logout = async () => {
    localStorage.removeItem("user");
};

export const getCurrentUser = () => {
    const userStr = localStorage.getItem("user");
    if (userStr) {
        let user = JSON.parse(userStr);
        return user;
    };
    return null;
};
export const getUsersList = () => {
    return axios.get(USER_URL, { headers: authHeader() });
};
