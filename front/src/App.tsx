import React, { useEffect, useState } from "react";

import { Layout, Menu, MenuProps } from "antd";

import {
  Navigate,
  Route,
  Routes,
  useLocation,
  useNavigate,
} from "react-router-dom";
import "./App.css";

import Sider from "antd/es/layout/Sider";
import { toast } from "react-toastify";
import errorManagement from "./common/ErrorHandler";
import EventBus from "./common/EventBus";
import BoardAnimal from "./components/board-animal.component";
import BoardUser from "./components/board-user.component";
import Home from "./components/home.component";
import Login from "./components/login.component";
import Profile from "./components/profile.component";
import Register from "./components/register.component";
import * as AuthService from "./services/auth.service";
import IUser from "./types/user.type";
const { Header, Content } = Layout;
const publicMenus = [
  {
    label: "Home",
    key: "home",
  },
];

const privateMenus = [
  ...publicMenus,
  {
    label: "Animal",
    key: "animal",
  },
  {
    label: "User",
    key: "user",
  },
];

const App: React.FC = () => {
  // User
  const [currentUser, setCurrentUser] = useState<IUser | undefined>(undefined);
  const [menus, setMenus] = useState<any>();
  const location = useLocation();
  const navigate = useNavigate();

  // Manage validation
  useEffect(() => {
    const user = AuthService.getCurrentUser();

    if (user) {
      AuthService.exist(user.id)
        .then(() => {
          setCurrentUser(user);
          navigate(location.pathname);
        })
        .catch((error) => {
          errorManagement(error);
        });
    }

    EventBus.on("login", logIn);
    EventBus.on("logout", logOut);

    return () => {
      EventBus.remove("login", logIn);
      EventBus.remove("logout", logOut);
    };
  }, [navigate, location.pathname]);

  // Set menus based on current user
  useEffect(() => {
    if (currentUser) {
      setMenus(privateMenus);
    } else {
      setMenus(publicMenus);
    }
  }, [currentUser]);

  // Function utils
  const logIn = () => {
    toast.success("Welcome !", {
      toastId: "welcome",
      position: toast.POSITION.BOTTOM_RIGHT,
    });
    setCurrentUser(AuthService.getCurrentUser());
  };

  const logOut = () => {
    AuthService.logout();
    setCurrentUser(undefined);
  };

  // Page
  const [current, setCurrent] = useState<string>("home");
  const onClick: MenuProps["onClick"] = (e) => {
    if (e.key === "login" && currentUser) {
      logOut();
    }

    setCurrent(e.key);
    navigate(e.key);
  };

  return (
    <Layout style={{ minHeight: "100vh" }}>
      <Header>
        {currentUser ? (
          <Menu
            onClick={onClick}
            selectedKeys={[current]}
            mode="horizontal"
            style={{ justifyContent: "flex-end" }}
            items={[
              {
                label: currentUser.username,
                key: "profile",
              },
              {
                label: "Logout",
                key: "login",
              },
            ]}
          ></Menu>
        ) : (
          <Menu
            onClick={onClick}
            selectedKeys={[current]}
            mode="horizontal"
            style={{ justifyContent: "flex-end" }}
            items={[
              {
                label: "Register",
                key: "register",
              },
              {
                label: "Login",
                key: "login",
              },
            ]}
          ></Menu>
        )}
      </Header>

      <Layout>
        <Sider>
          <Menu
            onClick={onClick}
            selectedKeys={[current]}
            mode="vertical"
            items={menus}
          ></Menu>
        </Sider>

        <Content>
          <Routes>
            {/* User */}
            <Route path="/" element={<Home />} />
            <Route path="/home" element={<Home />} />
            <Route path="/login" element={<Login />} />
            <Route path="/register" element={<Register />} />
            {/* Board */}
            <Route
              path="/profile"
              element={currentUser ? <Profile /> : <Navigate to="/login" />}
            />
            <Route
              path="/animal"
              element={currentUser ? <BoardAnimal /> : <Navigate to="/login" />}
            />
            <Route
              path="/user"
              element={currentUser ? <BoardUser /> : <Navigate to="/login" />}
            />
          </Routes>
        </Content>
      </Layout>
    </Layout>
  );
};

export default App;
