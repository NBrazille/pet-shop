import { toast } from "react-toastify";
import EventBus from "./EventBus";

export default function errorManagement(error: any) {
    if (error.response.status === 401) {
        toast.error("Session expirée !", {
            toastId: "expired",
            position: toast.POSITION.BOTTOM_RIGHT,
        });
        EventBus.dispatch("logout");
    }
}