#!/bin/bash
# Utilities for development

# Settings
APP_SERVICE=app
APP_CONTAINER=app

# Import environ variables
set -a
source .env
source .secrets
set +

# Update with external network
export DJANGO_DATABASE_HOST=0.0.0.0
export DJANGO_DATABASE_PORT=5433

# Functions
start() {
    # I do not why a process still exist after i shutdown my computer
    sudo kill $(sudo lsof -i :$DJANGO_DATABASE_PORT -t)
    
    docker compose up -d
    npm start --prefix front
}

stop() {
    docker compose stop
}

logs() {
    docker compose logs -f
}

manage() {
    docker exec -it $APP_CONTAINER python manage.py $@
}

migrate() {
    manage migrate $@
}

shell() {
    manage shell
}

install() {
    migrate
    manage createsuperuser --noinput
}

rebuild_app() {
    docker compose up -d --no-deps --build $APP_SERVICE
}

test_app() {
    cd src-back
    pytest
}

lint_app() {
    cd src-back
    pylint ./ --recursive=y
}

docs() {
    mkdocs serve
}

CMD_LIST=("start" "stop" "logs" "manage" "migrate" "shell" "install" "rebuild_app" "test_app" "lint_app" "docs")

# Help
help() {
    echo """Usage: $0 COMMAND

COMMAND :"""
    for elt in "${CMD_LIST[@]}"
    do
        echo "    $elt"
    done
    echo

    exit $1
}

# Arguments
command=$1
shift # Pop command from argument

if ! [[ " ${CMD_LIST[*]} " == *" $command "* ]]; then
    echo "Unknown command : $command"
    help 1
fi

$command
