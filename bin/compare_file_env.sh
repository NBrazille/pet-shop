#!/bin/bash

# When we develop a feature, it is a good practice to put env variables in an external file.
# It is even one criteria to see your application is Cloud-Ready or not.
# However, it is also common to forget to declare all variables in a deployment script or even in a documentation
# because we should not commit an .env since it depend on your local environment.
# So, i created this script and some <file>_example to help others to force to document all variables needed.

compare() {
    # Search variables in first file
    declare -a variables_1
    while read ligne; do
        variables_1+=("$ligne")
    done < <(awk -F= '{print $1}' $1)

    # Search variables in second file
    declare -a variables_2
    while read ligne; do
        variables_2+=("$ligne")
    done < <(awk -F= '{print $1}' $2)

    # Check if variables declared in first file is in second file
    declare -a diff
    for variable in "${variables_1[@]}"; do
        if ! [[ "${variables_2[*]}" =~ $variable ]]; then
            diff+=("$variable")
        fi
    done

    # Exit script with error if diff
    if ! [[ ${#diff[@]} -eq 0 ]]; then
        echo "Following variables in '$1' are not listed in '$2'"
        printf '%s\n' "    ${diff[@]}"
        exit 1
    fi
}

# Compare .env/.env_example to see if new variables are well configured
compare .env .env_example

# Compare .secrets/.secrets_example to see if new variables are well configured
compare .secrets .secrets_example
