#!/bin/bash

# Get current message
message=`cat $1`

# Get current branch
branch=$(git rev-parse --abbrev-ref HEAD)

# If message does not begin by branch, change it
if ! [[ "$message" =~ ^"[$branch]".* ]]; then
    echo "[$branch] $message" > $1
fi
