# Error starting userland proxy: listen tcp4 0.0.0.0:<Port>: bind: address already in use

Get process used by <port>

`sudo lsof -i :<port>`

Kill process

`sudo kill <pid>`
