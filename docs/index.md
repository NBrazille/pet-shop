# Pet Shop

Au coeur de l'application se trouve nos amis les bêtes.
Pour certaines personnes, simples animaux de compagnie. Pour d'autres, membres importants de la famille.

## Animal

On identifie un animal par :

- Son nom
- Son gardien principal
- Des gardiens mandataires
- Sa date de naissance
- Sa date d'enregistrement
- Son espèce (Chat, Cochon, Serpent, etc...)

:fontawesome-solid-circle-info: Pour ce projet vitrine, ce sera le modèle de données principal, auquel tout gravite.

## Gardien

On entend par "Gardien" un représentant légal garant du bien-être d'un ou plusieurs animaux.
Le cas le plus commun est une famille hébergeant un chat, un chien et/ou des poissons.

Cependant, je prend également en compte les zoos, les associations ou tout autre entité associée.

:fontawesome-solid-circle-info: Pour ce projet vitrine, ce seront les users qui peuvent se connecter à l'application.

## Nourriture et Co

Bien entendu, ces bêtes ont besoin de nourriture et d'accessoires pour que leur vie soit le plus confortable possible.
On listera bien entendu les croquettes, les patés, les grains vendus sur le marché mais également les bols, les paniers, les jouets ou encore les aquariums, etc ...

:fontawesome-solid-circle-info: Cette partie du projet démontre ma capacité à gérer de la donnée provenant de diverses sources.
Bien entendu, les quelques opérations d'achats seront fictives.

## Santé

Même si on accorde toute notre attention à ces bêtes, il y a des choses qu'on ne peut gérer en tant que particulier.

### Vétérinaire et co

On référencera ici quelques professionels de santé animalière (vétérinaire, maréchal-ferrant, etc ...)
On y trouvera des numéros de contact mais également des diplomes/certificats pour s'occuper de tel ou tel animal.

### Dossier médical

Accessible depuis la fiche de l'animal, on présentera les opérations que l'animal a subi durant sa vie (Vaccin, Chirurgie, etc ...).

## Ce que l'on ne prendra pas en compte dans ce projet

Etant donné que c'est un projet vitrine, je ne vais pas réellement prendre en compte tout ce qui concerne les animaux.
Bien que l'exercice soit intéressant, j'ai également besoin de limiter le périmètre afin de me concentrer sur le véritable but du projet.

- Pour les grosses entités (comme les zoos), on ne prendra pas compte la gestion des enclos, des salariés ... Bref, la gestion d'une exploitation entière.
- Les évènements (Concours, Salons des chiots, etc ...)
